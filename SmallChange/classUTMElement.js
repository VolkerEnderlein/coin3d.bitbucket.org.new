var classUTMElement =
[
    [ "~UTMElement", "classUTMElement.html#a14ff5fe09b01b31cd1cf5ed3030deaf5", null ],
    [ "copyMatchInfo", "classUTMElement.html#ab8502c6c5b520505f60e81ed3c84abb1", null ],
    [ "getCurrentTranslation", "classUTMElement.html#a744cf68d9630176b8c1bdb0afb83bd0d", null ],
    [ "getGlobalTransform", "classUTMElement.html#a957920c31fd07e44c351c8e7aa6d517b", null ],
    [ "getReferencePosition", "classUTMElement.html#a737236753eb04d32447b12aa303dfcfd", null ],
    [ "init", "classUTMElement.html#a25ec4eddf6d42aa9bc37bf32587c49ba", null ],
    [ "initClass", "classUTMElement.html#a4f9a1c269f8e8f772bd4d6d70c723bd3", null ],
    [ "matches", "classUTMElement.html#a318ee72359a1ddaa19e1e96b2ba3fa78", null ],
    [ "push", "classUTMElement.html#abc6a10d74d278fc6ebc9252f593ed06a", null ],
    [ "setGlobalTransform", "classUTMElement.html#a355cccf0f4d8e2c388f9c069565d3b91", null ],
    [ "setPosition", "classUTMElement.html#a85e6c63a8f11733138eb5e74f6c6e818", null ],
    [ "setReferencePosition", "classUTMElement.html#a4b55a042938e0640fceeda35deb2df7a", null ],
    [ "currtrans", "classUTMElement.html#aac15724d1606af91859d3a3abec504c1", null ],
    [ "easting", "classUTMElement.html#ae0380fe07676b0a5ec7ef9f4ecf11ba7", null ],
    [ "elevation", "classUTMElement.html#a7bfb24e80a9088ce4c2294a2254ee55e", null ],
    [ "gtransform", "classUTMElement.html#ae2cf3938390233a65f28d13c9eb34246", null ],
    [ "northing", "classUTMElement.html#a8f6b93870fd7c4df1318ad00d6b36e9b", null ]
];