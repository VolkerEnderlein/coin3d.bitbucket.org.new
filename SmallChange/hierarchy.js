var hierarchy =
[
    [ "HQSphereGenerator", "classHQSphereGenerator.html", null ],
    [ "HQSphereGenerator::object", "classHQSphereGenerator_1_1object.html", null ],
    [ "SbBox3< Type >", "classSbBox3.html", null ],
    [ "SbCubicSpline", "classSbCubicSpline.html", null ],
    [ "SbHash< Type, Key >", "classSbHash.html", null ],
    [ "SbHashEntry< Type, Key >", "classSbHashEntry.html", null ],
    [ "SbImage", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSbImage.html", [
      [ "SmTextureFont::FontImage", "classSmTextureFont_1_1FontImage.html", null ]
    ] ],
    [ "SbVec3< Type >", "classSbVec3.html", null ],
    [ "SmEnvelope", "classSmEnvelope.html", null ],
    [ "SmHash< Type, Key >", "classSmHash.html", null ],
    [ "SmHashEntry< Type, Key >", "classSmHashEntry.html", null ],
    [ "SmTextureFontBundle", "classSmTextureFontBundle.html", null ],
    [ "SoAction", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoAction.html", [
      [ "SmToVertexArrayShapeAction", "classSmToVertexArrayShapeAction.html", null ],
      [ "SoGenerateSceneGraphAction", "classSoGenerateSceneGraphAction.html", null ],
      [ "SoTweakAction", "classSoTweakAction.html", null ]
    ] ],
    [ "SoBase", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoBase.html", [
      [ "SoFieldContainer", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoFieldContainer.html", [
        [ "SoEngine", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoEngine.html", [
          [ "CubicSplineEngine", "classCubicSplineEngine.html", null ],
          [ "Rot2Heading", "classRot2Heading.html", null ],
          [ "SmInverseRotation", "classSmInverseRotation.html", null ]
        ] ],
        [ "SoNode", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoNode.html", [
          [ "CoinEnvironment", "classCoinEnvironment.html", null ],
          [ "SmBillboardClipPlane", "classSmBillboardClipPlane.html", null ],
          [ "SmDepthBuffer", "classSmDepthBuffer.html", null ],
          [ "SmEventHandler", "classSmEventHandler.html", [
            [ "SmExaminerEventHandler", "classSmExaminerEventHandler.html", [
              [ "SmSphereEventHandler", "classSmSphereEventHandler.html", null ]
            ] ],
            [ "SmHelicopterEventHandler", "classSmHelicopterEventHandler.html", null ],
            [ "SmPanEventHandler", "classSmPanEventHandler.html", null ]
          ] ],
          [ "SmTextureFont", "classSmTextureFont.html", null ],
          [ "SmTooltip", "classSmTooltip.html", null ],
          [ "SoBaseKit", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoBaseKit.html", [
            [ "DynamicNodeKit< SoBaseKit >", "classDynamicNodeKit.html", [
              [ "DynamicBaseKit", "classDynamicBaseKit.html", null ]
            ] ],
            [ "LegendKit", "classLegendKit.html", null ],
            [ "ShapeScale", "classShapeScale.html", null ],
            [ "SmAnnotationAxis", "classSmAnnotationAxis.html", null ],
            [ "SmAnnotationWall", "classSmAnnotationWall.html", null ],
            [ "SmAxisDisplayKit", "classSmAxisDisplayKit.html", null ],
            [ "SmAxisKit", "classSmAxisKit.html", null ],
            [ "SmCameraControlKit", "classSmCameraControlKit.html", null ],
            [ "SmDynamicObjectKit", "classSmDynamicObjectKit.html", null ],
            [ "SmGeoMarkerKit", "classSmGeoMarkerKit.html", null ],
            [ "SmNormalsKit", "classSmNormalsKit.html", null ],
            [ "SmPieChart", "classSmPieChart.html", null ],
            [ "SmPopupMenuKit", "classSmPopupMenuKit.html", null ],
            [ "SmTooltipKit", "classSmTooltipKit.html", null ],
            [ "SmTrackPointKit", "classSmTrackPointKit.html", null ],
            [ "SmWellLogKit", "classSmWellLogKit.html", null ],
            [ "SoCameraPathEditKit", "classSoCameraPathEditKit.html", null ],
            [ "SoFEMKit", "classSoFEMKit.html", null ],
            [ "SoInteractionKit", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoInteractionKit.html", [
              [ "SoDragger", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoDragger.html", [
                [ "SmRangeTranslate1Dragger", "classSmRangeTranslate1Dragger.html", null ],
                [ "SoAngle1Dragger", "classSoAngle1Dragger.html", null ]
              ] ]
            ] ]
          ] ],
          [ "SoCamera", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoCamera.html", [
            [ "SoPerspectiveCamera", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoPerspectiveCamera.html", [
              [ "SmViewpointWrapper", "classSmViewpointWrapper.html", null ],
              [ "UTMCamera", "classUTMCamera.html", null ]
            ] ]
          ] ],
          [ "SoFile", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoFile.html", [
            [ "AutoFile", "classAutoFile.html", null ],
            [ "SmLazyFile", "classSmLazyFile.html", null ]
          ] ],
          [ "SoGroup", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoGroup.html", [
            [ "PickCallback", "classPickCallback.html", null ],
            [ "SmSwitchboard", "classSmSwitchboard.html", [
              [ "SmSwitchboardOperator", "classSmSwitchboardOperator.html", null ]
            ] ],
            [ "SoSeparator", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoSeparator.html", [
              [ "SmTextureText2Collector", "classSmTextureText2Collector.html", null ]
            ] ],
            [ "SoSwitch", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoSwitch.html", [
              [ "PickSwitch", "classPickSwitch.html", null ]
            ] ]
          ] ],
          [ "SoLight", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoLight.html", [
            [ "SmHeadlight", "classSmHeadlight.html", null ]
          ] ],
          [ "SoShape", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoShape.html", [
            [ "Coinboard", "classCoinboard.html", null ],
            [ "SkyDome", "classSkyDome.html", null ],
            [ "SmHQSphere", "classSmHQSphere.html", null ],
            [ "SmTextureText2", "classSmTextureText2.html", null ],
            [ "SmTrack", "classSmTrack.html", null ],
            [ "SmVertexArrayShape", "classSmVertexArrayShape.html", null ],
            [ "SoLODExtrusion", "classSoLODExtrusion.html", null ],
            [ "SoTCBCurve", "classSoTCBCurve.html", null ],
            [ "SoText2", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoText2.html", [
              [ "SmShadowText2", "classSmShadowText2.html", null ]
            ] ],
            [ "SoText2Set", "classSoText2Set.html", null ],
            [ "SoVertexShape", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoVertexShape.html", [
              [ "InterleavedArraysShape", "classInterleavedArraysShape.html", null ],
              [ "SoNonIndexedShape", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoNonIndexedShape.html", [
                [ "SoPointCloud", "classSoPointCloud.html", null ],
                [ "SoPointSet", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoPointSet.html", [
                  [ "SoMarkerSet", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoMarkerSet.html", [
                    [ "SmMarkerSet", "classSmMarkerSet.html", null ]
                  ] ]
                ] ]
              ] ]
            ] ]
          ] ],
          [ "SoTransformation", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoTransformation.html", [
            [ "SmCoordinateSystem", "classSmCoordinateSystem.html", null ],
            [ "SoTransform", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoTransform.html", [
              [ "SoTransformManip", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoTransformManip.html", [
                [ "SoAngle1Manip", "classSoAngle1Manip.html", null ]
              ] ]
            ] ],
            [ "UTMPosition", "classUTMPosition.html", null ]
          ] ],
          [ "UTMCoordinate", "classUTMCoordinate.html", null ],
          [ "ViewportRegion", "classViewportRegion.html", null ]
        ] ]
      ] ]
    ] ],
    [ "SoElement", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoElement.html", [
      [ "GLDepthBufferElement", "classGLDepthBufferElement.html", null ],
      [ "SmTextureText2CollectorElement", "classSmTextureText2CollectorElement.html", null ],
      [ "SoReplacedElement", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoReplacedElement.html", [
        [ "SmTextureFontElement", "classSmTextureFontElement.html", null ]
      ] ],
      [ "UTMElement", "classUTMElement.html", null ]
    ] ],
    [ "SoSceneManager", "C:/Data/Volker/Bitbucket/installed/share/doc/Coin/html/classSoSceneManager.html", [
      [ "SmSceneManager", "classSmSceneManager.html", null ]
    ] ],
    [ "SmTextureText2CollectorElement::TextItem", "structSmTextureText2CollectorElement_1_1TextItem.html", null ],
    [ "HQSphereGenerator::triangle", "classHQSphereGenerator_1_1triangle.html", null ],
    [ "Base", null, [
      [ "DynamicNodeKit< Base >", "classDynamicNodeKit.html", null ]
    ] ]
];